# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150506063140) do

  create_table "cars", force: :cascade do |t|
    t.string   "make"
    t.string   "model"
    t.string   "vin"
    t.string   "color"
    t.integer  "year"
    t.integer  "msrp"
    t.integer  "markup"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "phone"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "customer_type_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "finances", force: :cascade do |t|
    t.integer  "quote_id"
    t.integer  "report_id"
    t.string   "payment_type"
    t.decimal  "interest"
    t.decimal  "period"
    t.string   "length"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "salesperson_id"
    t.integer  "customer_id"
    t.decimal  "sales_tax"
    t.decimal  "total"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "reports", force: :cascade do |t|
    t.decimal  "total_gross_rev"
    t.decimal  "net_profit"
    t.decimal  "sale_tax_total"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "sales_managers", force: :cascade do |t|
    t.integer  "sales_manager_id"
    t.string   "fname"
    t.string   "lname"
    t.string   "phone"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "salespeople", force: :cascade do |t|
    t.integer  "sales_manager_id"
    t.string   "fname"
    t.string   "lname"
    t.string   "phone"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

end
