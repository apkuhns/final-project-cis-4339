class CreateSalesManagers < ActiveRecord::Migration
  def change
    create_table :sales_managers do |t|
      t.integer :sales_manager_id
      t.string :fname
      t.string :lname
      t.string :phone

      t.timestamps null: false
    end
  end
end
