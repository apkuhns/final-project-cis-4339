class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :fname
      t.string :lname
      t.string :phone
      t.string :address
      t.string :city
      t.string :state
      t.string :customer_type_id

      t.timestamps null: false
    end
  end
end
