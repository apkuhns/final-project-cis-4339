class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :car_id
      t.integer :salesperson_id
      t.integer :customer_id
      t.decimal :sales_tax
      t.decimal :total

      t.timestamps null: false
    end
  end
end
