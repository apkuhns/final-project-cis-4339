class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.integer :quote_id
      t.integer :report_id
      t.string :payment_type
      t.decimal :interest
      t.decimal :period
      t.string :length

      t.timestamps null: false
    end
  end
end
