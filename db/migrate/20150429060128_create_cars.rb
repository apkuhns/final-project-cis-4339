class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :make
      t.string :model
      t.string :vin
      t.string :color
      t.integer :year
      t.integer :msrp
      t.integer :markup

      t.timestamps null: false
    end
  end
end
