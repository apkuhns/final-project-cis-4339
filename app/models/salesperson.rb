class Salesperson < ActiveRecord::Base

  has_many :quotes
  belongs_to :sales_manager

  def full_name
    "#{fname} #{lname}"
  end

  #validation
  validates :fname,
            presence: true
  validates :lname,
            presence: true
  validates :phone,
            presence: true,
            length: {minimum: 10, maximum: 11}

end
