class Quote < ActiveRecord::Base

  belongs_to :car
  belongs_to :customer
  has_one :finance
  belongs_to :salesperson

end
