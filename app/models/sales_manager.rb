class SalesManager < ActiveRecord::Base

  has_many :salespersons

  def full_name
    "#{fname} #{lname}"
  end
end
