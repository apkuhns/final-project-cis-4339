class Car < ActiveRecord::Base

  has_many :quotes
  before_save :set_markup

  #validation
  #validates :year,
        #    :numericality => { :greater_than_or_equal_to => 1900}



  private
  def set_markup
    self.markup = (msrp * 0.10) + msrp
  end
end
