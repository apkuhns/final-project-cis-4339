class Customer < ActiveRecord::Base

  has_many :quotes
  belongs_to :customer_type

  #validation
  validates :fname,
            presence: true
  validates :lname,
            presence: true
  validates :address,
            presence: true
  validates :phone,
            presence: true,
            length: {minimum: 10, maximum: 11}
  validates :state,
            presence: true
  validates :city,
            presence: true
end
