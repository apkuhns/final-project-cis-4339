json.array!(@quotes) do |quote|
  json.extract! quote, :id, :car_id, :salesperson_id, :customer_id, :sales_tax, :total
  json.url quote_url(quote, format: :json)
end
