json.array!(@customers) do |customer|
  json.extract! customer, :id, :fname, :lname, :phone, :address, :city, :state, :customer_type_id
  json.url customer_url(customer, format: :json)
end
