json.array!(@sales_managers) do |sales_manager|
  json.extract! sales_manager, :id, :sales_manager_id, :fname, :lname, :phone
  json.url sales_manager_url(sales_manager, format: :json)
end
