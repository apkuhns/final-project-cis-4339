json.array!(@cars) do |car|
  json.extract! car, :id, :make, :model, :vin, :color, :year, :msrp, :markup
  json.url car_url(car, format: :json)
end
