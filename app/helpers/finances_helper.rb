module FinancesHelper

  def period_num
    [
        ['3'],
        ['4'],
        ['5']
    ]
  end

  def payment_choices
    [
        ['Cash'],
        ['Payment Plan']
    ]
  end


end
